

#include "InfoMaxPlanning.h"

using namespace std;

InfoMaxPlanning::InfoMaxPlanning() {}
InfoMaxPlanning::~InfoMaxPlanning() {}

void InfoMaxPlanning::init(COORD_POS *pos) {
  double beliefAcc = 0;

  cout << "\n\n\t\t***Active Visual Search***\n" << endl;
  readPolicy();
  readObservation();
  readMap();
  this->coordPos = pos;
	actionCnt = 0;

  for (int i = 0; i < NUM_OF_STATES; i++) {
    belief[i] = 1.0 / NUM_OF_STATES;
    beliefAcc += belief[i];
  }

  // because of the possible bias, belief has to be normalized
  for (int i = 0; i < NUM_OF_STATES; i++) 
    belief[i] = belief[i] / beliefAcc; 
}

bool InfoMaxPlanning::readMap(){
  ifstream inFile;
  char str[128] = "";
  bool flag;
  inFile.open(mapFileName, ios::in);

  for (int i = 0; i < LEN_OF_EDGE_Y; i++)
    for (int j = 0; j < LEN_OF_EDGE_X; j++){
      inFile >> str;
      if (atoi(str) == 2)
        map[i][j] = false;
      else if (atoi(str) == 1)
        map[i][j] = true;
      else
        cout << "map reading error" << endl;
    }
  inFile >> str;
  cout << "Map reading finished..." << endl;
  return flag = (inFile.good()) ? false : true;
}

bool InfoMaxPlanning::readPolicy(){

  ifstream inFile;
  char word1[128] = "";
  char word2[128] = "";

  inFile.open(policyFileName, ios::in);

  inFile >> word1;
  inFile >> word2;
  if (atoi(word1) != NUM_OF_STATES + 1 || atoi(word2) != NUM_OF_STATES) {
    cout << "Policy file size confliction" << endl;
    return false;
  }

  for (int i = 0; i < NUM_OF_STATES + 1; i++){
    for (int j = 0; j < NUM_OF_STATES; j++){
      inFile >> word1;
      policyModel[i][j] = atof(word1);
      //cout << "policy " << i << " " << j << ": " << policyModel[i][j] << endl;
    }
  }

  inFile.close();

  cout << "Policy reading finished..." <<  endl;
  return true;;
}

bool InfoMaxPlanning::readObservation(){
  ifstream inFile;
  char str[128] = "";
  int cnt = 0;
    
  inFile.open(obsFileName);

  while (inFile.good()){
    strcpy(str, "");
    inFile >> str;
    if (!inFile.good())
      break;
    if (strncmp("O:a", str, 3) == 0){
      for (int i = 0; i < NUM_OF_STATES; i++){
        inFile >> str;
        obsModel[cnt][i][0] = atof(str);
        inFile >> str;
        obsModel[cnt][i][1] = atof(str);
        //cout << "Observation " << cnt << " " << i << " 0: " << obsModel[cnt][i][0] << endl;
        //cout << "Observation " << cnt << " " << i << " 1: " << obsModel[cnt][i][1] << endl;
      }
      cnt++;
    }
  }
  cout << "Observation reading finished..." <<  endl;
  inFile.close();
  return 1;
}

bool InfoMaxPlanning::checkIfFinished() {
  for (int i = 0; i < NUM_OF_STATES; i++)
    if (belief[i] > THRESHOLD)
      return true;
  return false;
}

void InfoMaxPlanning::vectorTimesMatrix( \
    double (&out)[NUM_OF_STATES], \
    double vec[NUM_OF_STATES], \
    double matrix[NUM_OF_STATES + 1][NUM_OF_STATES]){

  for (int i = 0; i < NUM_OF_STATES; i++)
    out[i] = 0;

  for (int i=0; i<NUM_OF_STATES; i++) {
    for (int j=0; j<NUM_OF_STATES; j++)
      out[i] = vec[j]*matrix[j][i] + out[i];
    out[i] = (SHIFT_FACTOR * matrix[NUM_OF_STATES][i]) + out[i];
  }
}

// Given the goal in grid map, for instance (2, 3), we want to get
// the real coordinates like (102.98, 23.78)
bool InfoMaxPlanning::getCoordPosByGridPos(GRID_POS *gridPos, COORD_POS *coordPos) {
  ifstream inFile;
  inFile.open(posListFileName, ios::in);
  int xx, yy;
  char str[128] = "";
  
  while (inFile.good()) {
    inFile >> str;
    xx = atoi(str);
    inFile >> str;
    yy = atoi(str);
    if (xx == gridPos->x && yy == gridPos->y && inFile.good()) {
      inFile >> str;
      coordPos->x = atof(str);
      inFile >> str;
      coordPos->y = atof(str);
      return true;
    }
    inFile >> str;
    inFile >> str;
  }
  return false;
}

void InfoMaxPlanning::selectAction(COORD_POS *goal) {

  double weightAcc[NUM_OF_STATES] = {0};
  double randNum;
  int x, y;
	
  srand(time(NULL));
  weightAcc[0] = weight[0];
  for (int i = 1; i < NUM_OF_STATES; i++)
    weightAcc[i] = weightAcc[i-1] + weight[i];

  for (int i = 0; i < NUM_OF_STATES; i++) 
    weightAcc[i] = weightAcc[i] / weightAcc[NUM_OF_STATES - 1];

  // Here we get a random number r, 0 < r < 1 
  randNum = (double)(rand()%10000)/10000;
	
  for (int i = 0; i < NUM_OF_STATES; i++) {
    if (weightAcc[i] > randNum) {
      x = (i % LEN_OF_EDGE_X) + 1;
      y = ceil( (i + 1.0) / LEN_OF_EDGE_X);
      action = i;
      break;
    }
  }

  gridPos = new GRID_POS;
  gridPos->x = x;
  gridPos->y = y;
  action = (gridPos->x - 1) + LEN_OF_EDGE_X * (gridPos->y - 1);

  cout << "Goal x: " << x << ", y: " << y << endl;
  if (!getCoordPosByGridPos(gridPos, goal))
    cout << "Grid pos to coordinate pos transfrom error" << endl;
  delete gridPos;

	actionCnt++;
}

double InfoMaxPlanning::getDistance(int firPt, int secPt){
  int xFirPt, yFirPt, xSecPt, ySecPt;

  firPt = firPt-1;
  secPt = secPt-1;
  xFirPt = firPt % LEN_OF_EDGE_X;
  yFirPt = (firPt - xFirPt) / LEN_OF_EDGE_X;
  xSecPt = secPt % LEN_OF_EDGE_X;
  ySecPt = (secPt - xSecPt) / LEN_OF_EDGE_X;
  return sqrt(pow((double)xFirPt-(double)xSecPt, 2) + pow((double)yFirPt-(double)ySecPt, 2));
}

void InfoMaxPlanning::getGridPosByCoordPos(COORD_POS *currPos, GRID_POS *gridPos) {
  ifstream inFile;
  char str[128] = "";
  int actionIndex;
  int xIndex, yIndex; // start from 1
  float xPosition, yPosition, distance;
  float minDistance = FLT_MAX;

  inFile.open(posListFileName, ios::in);
  while (inFile.good()) {
    inFile >> str;
    xIndex = atoi(str);
    inFile >> str;
    yIndex = atoi(str);
    inFile >> str;
    xPosition = atof(str);
    inFile >> str;
    yPosition = atof(str);
        
    distance = sqrt(pow(xPosition - currPos->x, 2) + \
        pow(yPosition - currPos->y, 2));
    if (distance < minDistance) {
      gridPos->x = xIndex;
      gridPos->y = yIndex;
      minDistance = distance;
    }
  }
  inFile.close();
}

void InfoMaxPlanning::updateBelief(const char name[128], VISION_PACK *vpack){
  if (0 == strcmp(name, vpack->name)) {
    obs = 1;
    cout << "I see the object" << endl;
  } else
    obs = 0;
  
  //getGridPosByCoordPos(this->coordPos, this->gridPos);

  beliefNormalizer = 0;
  for (int i = 0; i < NUM_OF_STATES; i++){
    belief[i] = obsModel[action][i][1-obs]*belief[i];
    beliefNormalizer += belief[i];
  }
  cout.precision(4);
  for (int i = 0; i < NUM_OF_STATES; i++) {
    belief[i] /= beliefNormalizer;
  }

  getWeights();
  preAction = action;
}

void InfoMaxPlanning::getWeights(){
  double distance;
  int x, y;

  vectorTimesMatrix(weight, belief, policyModel);
  for (int i = 0; i < NUM_OF_STATES; i++) {
    x = (i % LEN_OF_EDGE_X) + 1;
    y = ceil((i + 1.0)/LEN_OF_EDGE_X);
    //cout << "i: " << i << " " << "x: " << x << " " << "y: " << y << ", " << "map: "<< map[y-1][x-1] << endl;
    weight[i] = (map[y-1][x-1] == true) ? pow(EULER, weight[i]) : 0.0;
  }
  
  weightAcc = 0;
  for (int i = 0; i < NUM_OF_STATES; i++){
    distance = getDistance(i, preAction);
    weight[i] *= SPEED / (distance + 1);
    weightAcc += weight[i];
  }

  for (int i = 0; i < NUM_OF_STATES; i++)
    weight[i] /= weightAcc;
}

