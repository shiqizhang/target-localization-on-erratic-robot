#include <termios.h>
#include <signal.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/poll.h>

#include <boost/thread/thread.hpp>
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>

#define KEYCODE_W 0x77
#define KEYCODE_A 0x61
#define KEYCODE_S 0x73
#define KEYCODE_D 0x64

#define KEYCODE_A_CAP 0x41
#define KEYCODE_D_CAP 0x44
#define KEYCODE_S_CAP 0x53
#define KEYCODE_W_CAP 0x57

class ErraticKeyboardTeleopNode
{
    private:
        double walk_vel_;
        double run_vel_;
        double yaw_rate_;
        double yaw_rate_run_;
        
        geometry_msgs::Twist cmdvel_;
        ros::NodeHandle n_;
        ros::Publisher pub_;

    public:
        ErraticKeyboardTeleopNode()
        {
            pub_ = n_.advertise<geometry_msgs::Twist>("cmd_vel", 1);
            
            ros::NodeHandle n_private("~");
//            n_private.param("walk_vel", walk_vel_, 0.5);
//            n_private.param("run_vel", run_vel_, 1.0);
//            n_private.param("yaw_rate", yaw_rate_, 1.0);
//            n_private.param("yaw_rate_run", yaw_rate_run_, 1.5);
            n_private.param("walk_vel", walk_vel_, 0.2);
            n_private.param("run_vel", run_vel_, 0.5);
            n_private.param("yaw_rate", yaw_rate_, 0.4);
            n_private.param("yaw_rate_run", yaw_rate_run_, 0.8);
        }
        
        ~ErraticKeyboardTeleopNode() { }
        void keyboardLoop(VISION_PACK*, bool);
        
        void stopRobot()
        {
            cmdvel_.linear.x = 0.0;
            cmdvel_.angular.z = 0.0;
            pub_.publish(cmdvel_);
        }
};

ErraticKeyboardTeleopNode* tbk;
int kfd = 0;
struct termios cooked, raw;
bool done;

void turnAround(int argc, char** argv, VISION_PACK* vpack, bool isForceTurnAround)
{
    cout << "Em... I want to look around" << endl;
    ros::init(argc,argv,"tbk", ros::init_options::AnonymousName | ros::init_options::NoSigintHandler);
    ErraticKeyboardTeleopNode tbk;
    
//    boost::thread t = boost::thread(boost::bind(&ErraticKeyboardTeleopNode::keyboardLoop, &tbk));
//    ros::spin();
//    t.interrupt();
//    t.join();
//    tbk.stopRobot();
//    tcsetattr(kfd, TCSANOW, &cooked);

    tbk.keyboardLoop(vpack, isForceTurnAround);
    tbk.stopRobot();
    cout << "Looking around makes me feel good" << endl;
}

void ErraticKeyboardTeleopNode::keyboardLoop(VISION_PACK* vpack, bool isForceTurnAround)
{
    char c;
    double max_tv = walk_vel_;
    double max_rv = yaw_rate_;
    bool dirty = false;
    int speed = 0;
    int turn = 0;
    
    // get the console in raw mode
    tcgetattr(kfd, &cooked);
    memcpy(&raw, &cooked, sizeof(struct termios));
    raw.c_lflag &=~ (ICANON | ECHO);
    raw.c_cc[VEOL] = 1;
    raw.c_cc[VEOF] = 2;
    tcsetattr(kfd, TCSANOW, &raw);
    
    struct pollfd ufd;
    ufd.fd = kfd;
    ufd.events = POLLIN;
    
    for(int i = 0; i < 20; i++)
    {
        if (vpack->detected && !isForceTurnAround) {
          vpack->detected = false;

          cmdvel_.linear.x = speed * max_tv;
          cmdvel_.angular.z = -1 * turn * max_rv;
          pub_.publish(cmdvel_);
          cout << "# ";
          usleep(1500000); // turning

          cmdvel_.linear.x = 0.0;
          cmdvel_.angular.z = 0.0;
          pub_.publish(cmdvel_);

          break;
        }

        cmdvel_.linear.x = 0.0;
        cmdvel_.angular.z = 0.0;
        pub_.publish(cmdvel_);

        usleep(1000000); // standing
        boost::this_thread::interruption_point();

        max_rv = yaw_rate_;
        speed = 0;
        turn = 1;
        dirty = true;

        cmdvel_.linear.x = speed * max_tv;
        cmdvel_.angular.z = turn * max_rv;
        pub_.publish(cmdvel_);
        cout << "# ";
        usleep(1000000); // turning

        cmdvel_.linear.x = 0.0;
        cmdvel_.angular.z = 0.0;
        pub_.publish(cmdvel_);

    }
    cout << endl;
}

