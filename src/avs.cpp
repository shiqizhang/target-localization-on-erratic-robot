// 
// avs is the short for **<Active Visual Search>**
// 
// by Shiqi Zhang
// Department of Computer Science, Texas Tech University
// Oct. 2, 2011
//

#include <pthread.h>
#include "InfoMaxPlanning.h"
#include "vision.h"
#include "turnAround.h"
#include "util.h"

using namespace std;

InfoMaxPlanning *ip;
COORD_POS *coordPos, *coordGoal;
GRID_POS *gridPos, *gridGoal;
VISION_PACK *vPack;
char posName[128] = "";
bool hasFinished = false;
char words[256] = "echo";

int argC;
char **argV;

//void turnAround(int , char** );
void getPos() {}

void move() {
  ros::init(argC, argV, "simple_navigation_goals");
  actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> ac("move_base", true);
  while(!ac.waitForServer(ros::Duration(5.0))){
    ROS_INFO("Waiting for the move_base action server to come up");
  }
  move_base_msgs::MoveBaseGoal goal;

  goal.target_pose.header.frame_id = "map";
  goal.target_pose.header.stamp = ros::Time::now();

  goal.target_pose.pose.position.x = coordGoal->x;
  goal.target_pose.pose.position.y = coordGoal->y;
  //goal.target_pose.pose.orientation.w = coordPos->w;
  goal.target_pose.pose.orientation.w = 0.97;
  goal.target_pose.pose.orientation.z = -0.21;

  ac.sendGoal(goal);
  ac.waitForResult();

  if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED) {
    cout << "Arrived..." << endl;
  } else
    cout << "Not arrived" << endl;
}

void* planningThread(void* neverUsed) {
  ip = new InfoMaxPlanning();
  coordPos = new COORD_POS;
  coordGoal = new COORD_POS;
  gridPos = new GRID_POS;
  gridGoal = new GRID_POS;
  vPack = new VISION_PACK;
  bool isForceTurnAround = true;

  getPos(); // TODO
  ip->init(coordPos);
  cout << "\nWaiting for vision module to come up..." << endl;
  usleep(10000000); // to wait for visual information to come up

  while ( !(hasFinished = ip->checkIfFinished()) ) {

    ip->updateBelief(objName, vPack);
    ip->selectAction(coordGoal);
    move(); // move to coordGoal while observing


    vPack->detected = false;
    isForceTurnAround = true;
    turnAround(argC, argV, vPack, isForceTurnAround);
    if (vPack->detected) {
      strcpy(words, "echo \"I see the ");
      strcat(words, objName);
      strcat(words, "\" \| festival --tts &");
      system(words);
    } else {
      strcpy(words, "echo \"I do not see the ");
      strcat(words, objName);
      strcat(words, "\" \| festival --tts &");
      system(words);
    }
  }

  usleep(5000000);
  strcpy(words, "echo \"I found the ");
  strcat(words, objName);
  strcat(words, "\" \| festival --tts &");
  system(words);

  delete ip;
  delete coordPos;
  delete coordGoal;
  delete gridPos;
  delete gridGoal;
  delete vPack;
  return NULL;
  exit(1);
}

void* visionThread(void* neverUsed) {

  usleep(100000);
  cout << "entering vision module..." << endl;
  run_recognition(vPack);
  cout << "exiting vision module..." << endl;

  return NULL;
}

int main(int argc, char *argv[]){

  argC = argc;
  argV = argv;
  pthread_t pid1, pid2;

  pthread_create(&pid1, NULL, &planningThread, NULL);
  pthread_create(&pid2, NULL, &visionThread, NULL);
  pthread_join(pid1, NULL);
  pthread_join(pid2, NULL);
  
  return 1;
}



