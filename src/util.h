#ifndef UTIL_H
#define UTIL_H

#define LEN_OF_EDGE_X (20)
#define LEN_OF_EDGE_Y (8)
#define NUM_OF_STATES (LEN_OF_EDGE_X * LEN_OF_EDGE_Y)
#define SHIFT_FACTOR (-2.0)
#define EULER (2.7182818284590)
#define THRESHOLD (0.7)
#define SPEED (10)

// Only ONE object can be processed at a time
// Name of the object for localization
const static char objName[128] = "robot";

const char policyFileName[128] =  "/home/szhang/ros_workspace/avs/data/corridor.params";
const char obsFileName[128] =     "/home/szhang/ros_workspace/avs/data/corridor.POMDP";
const char posListFileName[128] = "/home/szhang/ros_workspace/avs/data/posList.txt";
const char mapFileName[128] =     "/home/szhang/ros_workspace/avs/data/corridor.map";

// center of soccer field
const int initialPos = 48; 

static const int FAST_THRESHOLD = 15; // the larger, the less keypoints
static const float RATIO_THRESHOLD = 0.7; // the larger, the less false positives
static const int NUM_MATCHES_THRESHOLD = 25;
static const int CAMERA_ID = 1; // 0 is the embedded webcam
static const char featureFileName[256] = "/home/szhang/ros_workspace/avs/data/features.txt";


typedef unsigned char byte;
struct VISION_PACK {
  char name[128];
  float distance;
  float reliability;
  unsigned int cnt;
  bool detected;
};
// For motion module
struct COORD_POS {
  float x;
  float y;
  float w;
};
struct GRID_POS {
  int x;
  int y;
};

#endif
