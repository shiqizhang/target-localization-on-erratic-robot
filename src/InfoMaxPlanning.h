#ifndef INFOMAXPLANNING_H
#define INFOMAXPLANNING_H

#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string.h>
#include <stdio.h>
#include <iomanip>
#include <math.h>
#include <float.h>
#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include "util.h"



class InfoMaxPlanning {
public:
  InfoMaxPlanning();
  ~InfoMaxPlanning();

  COORD_POS *coordPos, coordGoal;
  GRID_POS *gridPos, gridGoal;

  int action, preAction; // range from 0 to (NUM_OF_STATES - 1)
	unsigned int actionCnt;
  int obs; // 0 for nothing, and 1 for object detected
  double policyModel [NUM_OF_STATES + 1] [NUM_OF_STATES];
  double obsModel [NUM_OF_STATES] [NUM_OF_STATES] [2];

  // TRUE means 1:empty, FALSE for 2:obstacle
  bool map [LEN_OF_EDGE_Y] [LEN_OF_EDGE_X];   
  double belief[NUM_OF_STATES];
  double beliefNormalizer;
  double weight[NUM_OF_STATES];
  double weightAcc;

  void init(COORD_POS *);
  bool readPolicy();
  bool readObservation();
  bool readMap();
  bool checkIfFinished();
  void selectAction(COORD_POS *);
  void updateBelief(const char*, VISION_PACK *);
  void getWeights();
  double getDistance(int indexOfPos1, int indexOfPos2);
  //void getPosByName (ROB_POS *, char [128]);
  void vectorTimesMatrix(double (&out)[NUM_OF_STATES], double vec[NUM_OF_STATES], double matrix[NUM_OF_STATES + 1][NUM_OF_STATES]);
  void getGridPosByCoordPos(COORD_POS *, GRID_POS *);
  bool getCoordPosByGridPos(GRID_POS *, COORD_POS *);
};


#endif
